# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-16 00:48+0000\n"
"PO-Revision-Date: 2022-09-20 05:57+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "რუსუდან ცისკრელი"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Temuri.doghonadze@gmail.com"

#: currentcontainmentactionsmodel.cpp:201
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr "თაგუნას ქცევის დამატების მორგება"

#: main.cpp:88
#, kde-format
msgid "Plasma"
msgstr "Plasma"

#: main.cpp:88
#, kde-format
msgid "Plasma Shell"
msgstr "Plasma-ის სამუშაო მაგიდის გარსი"

#: main.cpp:100
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "QML-ის Javascript-ის გამმართველს ჩართვა"

#: main.cpp:103
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr "სიკვტილის შემდეგ plasma-shell ავტომატურად არ გაეშვება"

#: main.cpp:106
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "გარსის მითითებული დამატების ძალით ჩატვირთვა"

#: main.cpp:111
#, kde-format
msgid ""
"Load plasmashell as a standalone application, needs the shell-plugin option "
"to be specified"
msgstr ""
"Plasmashell-ის ცალკე აპლიკაციად გაშვება. საჭიროა მითითებული shell-plugin "
"პარამეტრი"

#: main.cpp:113
#, kde-format
msgid "Replace an existing instance"
msgstr "გაშვებული ასლის ჩანაცვლება"

#: main.cpp:116
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr "სატესტო რეჟიმის ჩართვა და განლაგების JavaScript ფაილის მითითება"

#: main.cpp:117
#, kde-format
msgid "file"
msgstr "ფაილი"

#: main.cpp:121
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "მომხმარებლის უკუკავშირის ხელმისაწვდომი ვარიანტები"

#: main.cpp:221
#, kde-format
msgid "Plasma Failed To Start"
msgstr "Plasma-ის გაშვების შეცდომა"

#: main.cpp:222
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""
"Plasma-ის გაშვების შეცდომა: OpenGL2-ის და პროგრამულ რენდერზე გადართვა "
"შეუძლებელია\n"
"შეამოწმეთ, თქვენი გრაფიკის დრაივერები სწორად თუა დაყენებული."

#: osd.cpp:54
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "აუდიო ჩაჩუმებულია"

#: osd.cpp:72
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr "მიკროფონი ჩაჩუმებულია"

#: osd.cpp:88
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 ჩაჩუმებულია"

#: osd.cpp:110
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr "თაჩპედის ჩართვა"

#: osd.cpp:112
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr "თაჩპედის გამორთვა"

#: osd.cpp:119
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr "Wifi ჩართულია"

#: osd.cpp:121
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr "Wifi გამორთულია"

#: osd.cpp:128
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr "Bluetooth ჩართულია"

#: osd.cpp:130
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr "Bluetooth გამორთულია"

#: osd.cpp:137
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr "მობილური ინტერნეტი ჩართულია"

#: osd.cpp:139
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr "მობილური ინტერნეტი გამორთულია"

#: osd.cpp:147
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr "კლავიატურა ეკრანზე ჩართულია"

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr "კლავიატურა ეკრანზე გამორთულია"

#: shellcontainmentconfig.cpp:129
#, kde-format
msgid "Unknown %1"
msgstr "უცნობი %1"

#: shellcorona.cpp:170 shellcorona.cpp:172
#, kde-format
msgid "Show Desktop"
msgstr "სამუშაო მაგიდის ჩვენება"

#: shellcorona.cpp:172
#, kde-format
msgid "Hide Desktop"
msgstr "სამუშაო მაგიდის დამალვა"

#: shellcorona.cpp:187
#, kde-format
msgid "Show Activity Switcher"
msgstr "აქტივობების გადამრთველის ჩვენება"

#: shellcorona.cpp:197
#, kde-format
msgid "Stop Current Activity"
msgstr "მიმდინარე აქტივობის შეჩერება"

#: shellcorona.cpp:204
#, kde-format
msgid "Switch to Previous Activity"
msgstr "წინა აქტივობაზე გადართვა"

#: shellcorona.cpp:211
#, kde-format
msgid "Switch to Next Activity"
msgstr "შემდეგ აქტივობაზე გადართვა"

#: shellcorona.cpp:226
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr "ამოცანების მენეჯერის ჩანაწერის აქტივაცია: %1"

#: shellcorona.cpp:252
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr "სამუშაო მაგიდებისა და პანელების მართვა..."

#: shellcorona.cpp:273
#, kde-format
msgid "Move keyboard focus between panels"
msgstr "კლავიატურის ფოკუსის პანელებს შორის გადატანა"

#: shellcorona.cpp:1795
#, kde-format
msgid "Add Panel"
msgstr "პანელის დამატება"

#: shellcorona.cpp:1831
#, kde-format
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr "%1-ის დაცარიელება"

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr "გამოიყენება პროგრამული რენდერერი"

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr "გამოიყენება პროგრამული რენდერერი"

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr "რენდერის წარმადობა შეიძლება დაეცეს"

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr "მეტჯერ აღარ აჩვენო ეს გაფრთხილება"

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr "პანელების რაოდენობა"

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr "პანელების რაოდენობა"
