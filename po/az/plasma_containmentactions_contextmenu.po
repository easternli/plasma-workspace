# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-11 00:47+0000\n"
"PO-Revision-Date: 2021-12-21 22:26+0400\n"
"Last-Translator: Kheyyam Gojayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: menu.cpp:99
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show KRunner"
msgstr "KRunner'i göstərmək"

#: menu.cpp:104
#, kde-format
msgid "Open Terminal"
msgstr ""

#: menu.cpp:108
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Lock Screen"
msgstr "Ekranı Kilidləmək"

#: menu.cpp:117
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Leave…"
msgstr "Tərk etmək..."

#: menu.cpp:126
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Display Settings…"
msgstr "Ekran ayarlarını tənzimləyin..."

#: menu.cpp:291
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Contextual Menu Plugin"
msgstr "Sağ düymə menyusu modulunu ayarlamaq"

#: menu.cpp:301
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Other Actions]"
msgstr "[Digər Əməllər]"

#: menu.cpp:304
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Wallpaper Actions"
msgstr "Divar Kağızı əməlləri"

#: menu.cpp:308
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Separator]"
msgstr "[Ayırıcı]"
