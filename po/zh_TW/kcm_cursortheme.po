# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin, 2015.
# Jeff Huang <s8321414@gmail.com>, 2016.
# pan93412 <pan93412@gmail.com>, 2018, 2019.
# Kisaragi Hiu <mail@kisaragi-hiu.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2022-06-29 11:36+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Traditional Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.04.2\n"

#. i18n: ectx: label, entry (cursorTheme), group (Mouse)
#: cursorthemesettings.kcfg:9
#, kde-format
msgid "Name of the current cursor theme"
msgstr "目前的游標主題名稱"

#. i18n: ectx: label, entry (cursorSize), group (Mouse)
#: cursorthemesettings.kcfg:13
#, kde-format
msgid "Current cursor size"
msgstr "目前游標大小"

#: kcmcursortheme.cpp:302
#, kde-format
msgid ""
"You have to restart the Plasma session for these changes to take effect."
msgstr "您必須重新啟動 Plasma 作業階段才能讓這些改變生效。"

#: kcmcursortheme.cpp:376
#, kde-format
msgid "Unable to create a temporary file."
msgstr "無法建立暫存檔案。"

#: kcmcursortheme.cpp:387
#, kde-format
msgid "Unable to download the icon theme archive: %1"
msgstr "無法下載圖示主題存檔：%1"

#: kcmcursortheme.cpp:418
#, kde-format
msgid "The file is not a valid icon theme archive."
msgstr "這個檔案不是有效的檔案主題存檔。"

#: kcmcursortheme.cpp:425
#, kde-format
msgid "Failed to create 'icons' folder."
msgstr "無法建立「icons」目錄。"

#: kcmcursortheme.cpp:434
#, kde-format
msgid ""
"A theme named %1 already exists in your icon theme folder. Do you want "
"replace it with this one?"
msgstr "名為 %1 的主題已存在您的圖示主題資料夾。您要取代它嗎？"

#: kcmcursortheme.cpp:438
#, kde-format
msgid "Overwrite Theme?"
msgstr "要覆寫主題嗎？"

#: kcmcursortheme.cpp:462
#, kde-format
msgid "Theme installed successfully."
msgstr "主題安裝成功。"

#: package/contents/ui/Delegate.qml:54
#, kde-format
msgid "Remove Theme"
msgstr "移除主題"

#: package/contents/ui/Delegate.qml:61
#, kde-format
msgid "Restore Cursor Theme"
msgstr "還原游標主題"

#: package/contents/ui/main.qml:20
#, kde-format
msgid "This module lets you choose the mouse cursor theme."
msgstr "此模組能讓您選擇滑鼠游標主題。"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Size:"
msgstr "大小："

#: package/contents/ui/main.qml:134
#, kde-format
msgid "&Install from File…"
msgstr "從檔案安裝(&I)…"

#: package/contents/ui/main.qml:140
#, kde-format
msgid "&Get New Cursors…"
msgstr "取得新游標(&G)…"

#: package/contents/ui/main.qml:157
#, kde-format
msgid "Open Theme"
msgstr "開啟主題"

#: package/contents/ui/main.qml:159
#, kde-format
msgid "Cursor Theme Files (*.tar.gz *.tar.bz2)"
msgstr "游標主題檔案 (*.tar.gz *.tar.bz2)"

#: plasma-apply-cursortheme.cpp:42
#, kde-format
msgid "The requested size '%1' is not available, using %2 instead."
msgstr "您要求設定的大小「%1」不可用，已改用 %2。"

#: plasma-apply-cursortheme.cpp:63
#, kde-format
msgid ""
"This tool allows you to set the mouse cursor theme for the current Plasma "
"session, without accidentally setting it to one that is either not "
"available, or which is already set."
msgstr ""
"此工具讓您能為目前 Plasma 工作階段設定滑鼠游標主題，而不會意外套用無法使用或"
"是已經設定的游標主題。"

#: plasma-apply-cursortheme.cpp:67
#, kde-format
msgid ""
"The name of the cursor theme you wish to set for your current Plasma session "
"(passing a full path will only use the last part of the path)"
msgstr ""
"您希望目前 Plasma 工作階段使用的游標主題名稱（填入完整路徑時只會使用路徑最後"
"的部分）"

#: plasma-apply-cursortheme.cpp:68
#, kde-format
msgid ""
"Show all the themes available on the system (and which is the current theme)"
msgstr "顯示系統上可用的主題（以及目前使用的主題）"

#: plasma-apply-cursortheme.cpp:69
#, kde-format
msgid "Use a specific size, rather than the theme default size"
msgstr "用指定的大小，而非主題預設大小"

#: plasma-apply-cursortheme.cpp:90
#, kde-format
msgid ""
"The requested theme \"%1\" is already set as the theme for the current "
"Plasma session."
msgstr "您要求設定的主題「%1」已經是目前 Plasma 工作階段的主題了。"

#: plasma-apply-cursortheme.cpp:101
#, kde-format
msgid ""
"Successfully applied the mouse cursor theme %1 to your current Plasma session"
msgstr "成功將滑鼠游標主題「%1」套用至目前的 Plasma 工作階段"

#: plasma-apply-cursortheme.cpp:103
#, kde-format
msgid ""
"You have to restart the Plasma session for your newly applied mouse cursor "
"theme to display correctly."
msgstr "您必須重新啟動 Plasma 作業階段才能讓剛套用的滑鼠游標主題正確的顯示。"

#: plasma-apply-cursortheme.cpp:113
#, kde-format
msgid ""
"Could not find theme \"%1\". The theme should be one of the following "
"options: %2"
msgstr "找不到主題「%1」。主題應該要是以下選項之一：%2"

#: plasma-apply-cursortheme.cpp:121
#, kde-format
msgid "You have the following mouse cursor themes on your system:"
msgstr "您的系統上有以下的滑鼠游標主題："

#: plasma-apply-cursortheme.cpp:126
#, kde-format
msgid "(Current theme for this Plasma session)"
msgstr "（此 Plasma 工作階段目前所使用的主題）"

#: xcursor/xcursortheme.cpp:60
#, kde-format
msgctxt ""
"@info The argument is the list of available sizes (in pixel). Example: "
"'Available sizes: 24' or 'Available sizes: 24, 36, 48'"
msgid "(Available sizes: %1)"
msgstr "（可用大小：%1）"

#~ msgid "Name"
#~ msgstr "名稱"

#~ msgid "Description"
#~ msgstr "描述"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Franklin Weng, Jeff Huang"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "franklin@goodhorse.idv.tw, s8321414@gmail.com"

#~ msgid "Cursors"
#~ msgstr "游標"

#~ msgid "(c) 2003-2007 Fredrik Höglund"
#~ msgstr "(c) 2003-2007 Fredrik Höglund"

#~ msgid "Fredrik Höglund"
#~ msgstr "Fredrik Höglund"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid ""
#~ "<qt>You cannot delete the theme you are currently using.<br />You have to "
#~ "switch to another theme first.</qt>"
#~ msgstr ""
#~ "<qt>您不能刪除目前正在使用的主題。<br />您必須先切換到其他主題。</qt>"

#~ msgid ""
#~ "<qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This "
#~ "will delete all the files installed by this theme.</qt>"
#~ msgstr ""
#~ "<qt>您確定要移除 <i>%1</i> 主題嗎？<br/>這樣會刪除這個主題所安裝的所有檔"
#~ "案。</qt>"

#~ msgid "Confirmation"
#~ msgstr "確認"

#~ msgctxt "@item:inlistbox size"
#~ msgid "Resolution dependent"
#~ msgstr "基於解析度"
