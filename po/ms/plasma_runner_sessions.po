# plasma_runner_sessions Bahasa Melayu (Malay) (ms)
# Copyright (C) 2008, 2009 K Desktop Environment
# This file is distributed under the same license as the krunner_sessions package.
#
# Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>, 2008, 2009, 2010.
# Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_sessions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-26 00:48+0000\n"
"PO-Revision-Date: 2011-06-23 22:51+0800\n"
"Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>\n"
"Language-Team: Malay <kedidiemas@yahoogroups.com>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=1;\n"

#: sessionrunner.cpp:28 sessionrunner.cpp:68
#, kde-format
msgctxt "log out command"
msgid "logout"
msgstr "logkeluar"

#: sessionrunner.cpp:28
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr "Log keluar, mematikan sesi semasa desktop"

#: sessionrunner.cpp:29 sessionrunner.cpp:85
#, fuzzy, kde-format
#| msgctxt "shutdown computer command"
#| msgid "shutdown"
msgctxt "shut down computer command"
msgid "shut down"
msgstr "matikan"

#: sessionrunner.cpp:29
#, kde-format
msgid "Turns off the computer"
msgstr "Matikan komputer"

#: sessionrunner.cpp:33 sessionrunner.cpp:94
#, kde-format
msgctxt "lock screen command"
msgid "lock"
msgstr "kunci"

#: sessionrunner.cpp:33
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "Kunci sesi semasa dan mulakan penghias skrin"

#: sessionrunner.cpp:36 sessionrunner.cpp:76
#, kde-format
msgctxt "restart computer command"
msgid "restart"
msgstr "ulangmula"

#: sessionrunner.cpp:36
#, kde-format
msgid "Reboots the computer"
msgstr "Ulangmula komputer"

#: sessionrunner.cpp:37 sessionrunner.cpp:77
#, kde-format
msgctxt "restart computer command"
msgid "reboot"
msgstr "ulangmula"

#: sessionrunner.cpp:40 sessionrunner.cpp:104
#, kde-format
msgctxt "save session command"
msgid "save"
msgstr ""

#: sessionrunner.cpp:40
#, kde-format
msgid "Saves the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:41 sessionrunner.cpp:105
#, fuzzy, kde-format
#| msgid "new session"
msgctxt "save session command"
msgid "save session"
msgstr "sesi baru"

#: sessionrunner.cpp:44
#, kde-format
msgctxt "switch user command"
msgid "switch"
msgstr "tukar"

#: sessionrunner.cpp:45
#, kde-format
msgctxt "switch user command"
msgid "switch :q:"
msgstr "tukar :q:"

#: sessionrunner.cpp:46
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""
"Menukar ke sesi aktif untuk pengguna :q:, atau senaraikan semua sesi aktif "
"jika :q: tidak diberikan"

#: sessionrunner.cpp:49 sessionrunner.cpp:144
#, kde-format
msgid "switch user"
msgstr "tukar pengguna"

#: sessionrunner.cpp:49
#, kde-format
msgid "Starts a new session as a different user"
msgstr "Mulakan sesi baru sebagai pengguna lain"

#: sessionrunner.cpp:50 sessionrunner.cpp:144
#, kde-format
msgid "new session"
msgstr "sesi baru"

#: sessionrunner.cpp:54
#, kde-format
msgid "Lists all sessions"
msgstr "Senaraikan semua sesi"

#: sessionrunner.cpp:68
#, kde-format
msgid "log out"
msgstr "log keluar"

#: sessionrunner.cpp:70
#, kde-format
msgctxt "log out command"
msgid "Logout"
msgstr "Logkeluar"

#: sessionrunner.cpp:79
#, kde-format
msgid "Restart the computer"
msgstr "Ulangmula komputer"

#: sessionrunner.cpp:86
#, fuzzy, kde-format
#| msgctxt "shutdown computer command"
#| msgid "shutdown"
msgctxt "shut down computer command"
msgid "shutdown"
msgstr "matikan"

#: sessionrunner.cpp:88
#, fuzzy, kde-format
#| msgid "Shutdown the computer"
msgid "Shut down the computer"
msgstr "Matikan komputer"

#: sessionrunner.cpp:97
#, kde-format
msgid "Lock the screen"
msgstr "Kunci skrin"

#: sessionrunner.cpp:107
#, fuzzy, kde-format
#| msgid "new session"
msgid "Save the session"
msgstr "sesi baru"

#: sessionrunner.cpp:108
#, kde-format
msgid "Save the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:129
#, kde-format
msgctxt "User sessions"
msgid "sessions"
msgstr "sesi"

#: sessionrunner.cpp:150
#, fuzzy, kde-format
#| msgid "switch user"
msgid "Switch User"
msgstr "tukar pengguna"

#: sessionrunner.cpp:227
#, kde-format
msgid "New Session"
msgstr "Sesi Baru"

#: sessionrunner.cpp:228
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type 'switch' or 'sessions')</li><li>Plasma widgets "
"(such as the application launcher)</li></ul>"
msgstr ""

#~ msgid "Warning - New Session"
#~ msgstr "Amaran - Sesi Baru"

#, fuzzy
#~| msgid ""
#~| "<p>You have chosen to open another desktop session.<br />The current "
#~| "session will be hidden and a new login screen will be displayed.<br />An "
#~| "F-key is assigned to each session; F%1 is usually assigned to the first "
#~| "session, F%2 to the second session and so on. You can switch between "
#~| "sessions by pressing Ctrl, Alt and the appropriate F-key at the same "
#~| "time. Additionally, the KDE Panel and Desktop menus have actions for "
#~| "switching between sessions.</p>"
#~ msgid ""
#~ "<p>You have chosen to open another desktop session.<br />The current "
#~ "session will be hidden and a new login screen will be displayed.<br />An "
#~ "F-key is assigned to each session; F%1 is usually assigned to the first "
#~ "session, F%2 to the second session and so on. You can switch between "
#~ "sessions by pressing Ctrl, Alt and the appropriate F-key at the same "
#~ "time. Additionally, the Plasma Panel and Desktop menus have actions for "
#~ "switching between sessions.</p>"
#~ msgstr ""
#~ "<p>Anda telah memilih untuk membuka sesi desktop lain.<br />Sesi semasa "
#~ "akan disembunyikan dan skrin logmasuk baru akan dipaparkan.<br />Kekunci-"
#~ "F diwakilkan kepada setiap sesi; F%1 biasanya diwakilkan kepada sesi "
#~ "pertama, F%2 kepada sesi kedua dan seterusnya. Anda boleh menukar antara "
#~ "sesi sengan menekan Ctrl, Alt dan kekunci-F yang sepatutnya pada masa "
#~ "yang sama. Tambahan lagi, Panel KDE dan menu Desktop mempunyai tindakan "
#~ "untuk menukar antara sesi.</p>"

#~ msgid "&Start New Session"
#~ msgstr "Mulakan &Sesi Baru"
