# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2022-04-21 16:04+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Fella Avatar Carson KWallet avatar Devin Lin\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/ChangePassword.qml:26
#: package/contents/ui/UserDetailsPage.qml:166
#, kde-format
msgid "Change Password"
msgstr "Mudar a Senha"

#: package/contents/ui/ChangePassword.qml:41
#, kde-format
msgid "Password"
msgstr "Senha"

#: package/contents/ui/ChangePassword.qml:55
#, kde-format
msgid "Confirm password"
msgstr "Confirmar a senha"

#: package/contents/ui/ChangePassword.qml:68
#: package/contents/ui/CreateUser.qml:65
#, kde-format
msgid "Passwords must match"
msgstr "As senhas devem ser iguais"

#: package/contents/ui/ChangePassword.qml:73
#, kde-format
msgid "Set Password"
msgstr "Definir a Senha"

#: package/contents/ui/ChangeWalletPassword.qml:17
#, kde-format
msgid "Change Wallet Password?"
msgstr "Mudar a Senha da Carteira?"

#: package/contents/ui/ChangeWalletPassword.qml:26
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"Agora que modificou a sua senha, poderá querer também mudar a senha na sua "
"KWallet predefinida para ficar consistente."

#: package/contents/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr "O que é o KWallet?"

#: package/contents/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"O KWallet é um gestor de senhas que guarda as suas senhas para as redes sem-"
"fios e outros recursos codificados. Está bloqueada com a sua própria senha, "
"a qual é diferente da senha da sua conta. Se as duas senhas corresponderem, "
"poderá ser desbloqueada logo no início da sessão, para que não tenha de "
"introduzir você mesmo a senha do KWallet."

#: package/contents/ui/ChangeWalletPassword.qml:57
#, kde-format
msgid "Change Wallet Password"
msgstr "Mudar a Senha da Carteira"

#: package/contents/ui/ChangeWalletPassword.qml:66
#, kde-format
msgid "Leave Unchanged"
msgstr "Deixar Inalterado"

#: package/contents/ui/CreateUser.qml:16
#, kde-format
msgid "Create User"
msgstr "Criar um Utilizador"

#: package/contents/ui/CreateUser.qml:30
#: package/contents/ui/UserDetailsPage.qml:134
#, kde-format
msgid "Name:"
msgstr "Nome:"

#: package/contents/ui/CreateUser.qml:34
#: package/contents/ui/UserDetailsPage.qml:141
#, kde-format
msgid "Username:"
msgstr "Utilizador:"

#: package/contents/ui/CreateUser.qml:44
#: package/contents/ui/UserDetailsPage.qml:149
#, kde-format
msgid "Standard"
msgstr "Normal"

#: package/contents/ui/CreateUser.qml:45
#: package/contents/ui/UserDetailsPage.qml:150
#, kde-format
msgid "Administrator"
msgstr "Administrador"

#: package/contents/ui/CreateUser.qml:48
#: package/contents/ui/UserDetailsPage.qml:153
#, kde-format
msgid "Account type:"
msgstr "Tipo de conta:"

#: package/contents/ui/CreateUser.qml:53
#, kde-format
msgid "Password:"
msgstr "Senha:"

#: package/contents/ui/CreateUser.qml:58
#, kde-format
msgid "Confirm password:"
msgstr "Confirmar a senha:"

#: package/contents/ui/CreateUser.qml:69
#, kde-format
msgid "Create"
msgstr "Criar"

#: package/contents/ui/FingerprintDialog.qml:57
#, kde-format
msgid "Configure Fingerprints"
msgstr "Configurar as Impressões Digitais"

#: package/contents/ui/FingerprintDialog.qml:67
#, kde-format
msgid "Clear Fingerprints"
msgstr "Limpar as Impressões Digitais"

#: package/contents/ui/FingerprintDialog.qml:74
#, kde-format
msgid "Add"
msgstr "Adicionar"

#: package/contents/ui/FingerprintDialog.qml:83
#: package/contents/ui/FingerprintDialog.qml:100
#, kde-format
msgid "Cancel"
msgstr "Cancelar"

#: package/contents/ui/FingerprintDialog.qml:89
#, kde-format
msgid "Continue"
msgstr "Continuar"

#: package/contents/ui/FingerprintDialog.qml:108
#, kde-format
msgid "Done"
msgstr "Concluído"

#: package/contents/ui/FingerprintDialog.qml:131
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "A Reconhecer a Impressão Digital"

#: package/contents/ui/FingerprintDialog.qml:137
#, kde-format
msgctxt ""
"%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgstr ""
"Por favor, %1 de forma repetida o seu %2 no sensor de impressões digitais."

#: package/contents/ui/FingerprintDialog.qml:147
#, kde-format
msgid "Finger Enrolled"
msgstr "Dedo Reconhecido"

#: package/contents/ui/FingerprintDialog.qml:183
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Escolha o dedo a reconhecer"

#: package/contents/ui/FingerprintDialog.qml:247
#, kde-format
msgid "Re-enroll finger"
msgstr "Reconhecer de novo o dedo"

#: package/contents/ui/FingerprintDialog.qml:265
#, kde-format
msgid "No fingerprints added"
msgstr "Sem impressões digitais adicionadas"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "Manage Users"
msgstr "Gerir os Utilizadores"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Add New User"
msgstr "Adicionar um Novo Utilizador"

#: package/contents/ui/UserDetailsPage.qml:89
#, kde-format
msgid "Choose a picture"
msgstr "Escolher uma imagem"

#: package/contents/ui/UserDetailsPage.qml:120
#, kde-format
msgid "Change avatar"
msgstr "Mudar o avatar"

#: package/contents/ui/UserDetailsPage.qml:162
#, kde-format
msgid "Email address:"
msgstr "Endereço de e-mail:"

#: package/contents/ui/UserDetailsPage.qml:186
#, kde-format
msgid "Delete files"
msgstr "Apagar os ficheiros"

#: package/contents/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Keep files"
msgstr "Manter os ficheiros"

#: package/contents/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Delete User…"
msgstr "Apagar o Utilizador…"

#: package/contents/ui/UserDetailsPage.qml:211
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Configurar a Autenticação por Impressão Digital…"

#: package/contents/ui/UserDetailsPage.qml:225
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Poderá usar as suas impressões digitais em vez de uma senha ao desbloquear o "
"ecrã ou ao dar permissões de administração às aplicações e programas da "
"linha de comandos que as solicitem.<nl/><nl/>A autenticação no sistema com a "
"sua impressão digital ainda não é suportada."

#: package/contents/ui/UserDetailsPage.qml:235
#, kde-format
msgid "Change Avatar"
msgstr "Mudar o Avatar"

#: package/contents/ui/UserDetailsPage.qml:238
#, kde-format
msgid "It's Nothing"
msgstr "Não é Nada"

#: package/contents/ui/UserDetailsPage.qml:239
#, kde-format
msgid "Feisty Flamingo"
msgstr "Flamingo Festivo"

#: package/contents/ui/UserDetailsPage.qml:240
#, kde-format
msgid "Dragon's Fruit"
msgstr "Fruta do Dragão"

#: package/contents/ui/UserDetailsPage.qml:241
#, kde-format
msgid "Sweet Potato"
msgstr "Batata Doce"

#: package/contents/ui/UserDetailsPage.qml:242
#, kde-format
msgid "Ambient Amber"
msgstr "Âmbar Ambiental"

#: package/contents/ui/UserDetailsPage.qml:243
#, kde-format
msgid "Sparkle Sunbeam"
msgstr "Raio do Sol"

#: package/contents/ui/UserDetailsPage.qml:244
#, kde-format
msgid "Lemon-Lime"
msgstr "Limão-Lima"

#: package/contents/ui/UserDetailsPage.qml:245
#, kde-format
msgid "Verdant Charm"
msgstr "Charme Verdejante"

#: package/contents/ui/UserDetailsPage.qml:246
#, kde-format
msgid "Mellow Meadow"
msgstr "Campo"

#: package/contents/ui/UserDetailsPage.qml:247
#, kde-format
msgid "Tepid Teal"
msgstr "Azul Tépido"

#: package/contents/ui/UserDetailsPage.qml:248
#, kde-format
msgid "Plasma Blue"
msgstr "Azul do Plasma"

#: package/contents/ui/UserDetailsPage.qml:249
#, kde-format
msgid "Pon Purple"
msgstr "Púrpura"

#: package/contents/ui/UserDetailsPage.qml:250
#, kde-format
msgid "Bajo Purple"
msgstr "Púrpura Baixo"

#: package/contents/ui/UserDetailsPage.qml:251
#, kde-format
msgid "Burnt Charcoal"
msgstr "Carvão Ardido"

#: package/contents/ui/UserDetailsPage.qml:252
#, kde-format
msgid "Paper Perfection"
msgstr "Perfeição do Papel"

#: package/contents/ui/UserDetailsPage.qml:253
#, kde-format
msgid "Cafétera Brown"
msgstr "Castanho de Cafeteira"

#: package/contents/ui/UserDetailsPage.qml:254
#, kde-format
msgid "Rich Hardwood"
msgstr "Madeira Rica"

#: package/contents/ui/UserDetailsPage.qml:305
#, kde-format
msgid "Choose File…"
msgstr "Escolher o Ficheiro…"

#: src/fingerprintmodel.cpp:150 src/fingerprintmodel.cpp:257
#, kde-format
msgid "No fingerprint device found."
msgstr "Não foi detectado nenhum dispositivo de impressões digitais."

#: src/fingerprintmodel.cpp:316
#, kde-format
msgid "Retry scanning your finger."
msgstr "Repita o reconhecimento do seu dedo."

#: src/fingerprintmodel.cpp:318
#, kde-format
msgid "Swipe too short. Try again."
msgstr "A passagem foi demasiado curta. Tente de novo."

#: src/fingerprintmodel.cpp:320
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "O dedo não está centrado no leitor. Tente de novo."

#: src/fingerprintmodel.cpp:322
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Remova o seu dedo do leitor e tente de novo."

#: src/fingerprintmodel.cpp:330
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Não foi possível fazer o reconhecimento da impressão digital."

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"Não há mais espaço disponível neste dispositivo. Apague outras impressões "
"digitais para continuar."

#: src/fingerprintmodel.cpp:336
#, kde-format
msgid "The device was disconnected."
msgstr "O dispositivo foi desligado."

#: src/fingerprintmodel.cpp:341
#, kde-format
msgid "An unknown error has occurred."
msgstr "Ocorreu um erro desconhecido."

#: src/user.cpp:267
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "Não foi possível obter permissões para gravar o utilizador %1"

#: src/user.cpp:272
#, kde-format
msgid "There was an error while saving changes"
msgstr "Ocorreu um erro ao gravar as alterações"

#: src/user.cpp:368
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr ""
"Não foi possível dimensionar a imagem: a abertura do ficheiro temporário "
"falhou"

#: src/user.cpp:376
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr ""
"Não foi possível dimensionar a imagem: a gravação no ficheiro temporário "
"falhou"

#: src/usermodel.cpp:138
#, kde-format
msgid "Your Account"
msgstr "A Sua Conta"

#: src/usermodel.cpp:138
#, kde-format
msgid "Other Accounts"
msgstr "Outras Contas"
